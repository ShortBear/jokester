from pydantic import BaseModel
from typing import Union
from queries.pool import pool

class Error(BaseModel):
    message: str

class DuplicateAccountError(Exception):
    message: str

class AccountOut(BaseModel):
    id: int
    username: str

class AccountIn(BaseModel):
    username: str
    password: str

class AccountOutWithPassword(AccountOut):
    hashed_password: str

class AccountRepo:
    def get(self, username) -> Union[AccountOutWithPassword, Error]:
        print('AccountRepo.get', username)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    query = db.execute(
                        """
                        SELECT id, username, password
                        FROM accounts
                        WHERE username = %s;
                        """,
                        [
                            username
                        ]
                    )
                    result = query.fetchone()
                    if result is None:
                        return {'message': "Can't find username"}
                    return AccountOutWithPassword(id=result[0], username=result[1], hashed_password=result[2])
        except Exception as e:
            print(e)
            return {'message': 'Could not get account'}

    def create(self, info: AccountIn, hashed_password: str) -> Union[AccountOutWithPassword, Error]:
        print('AccountRepo.create hashed_password', hashed_password)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts
                            (username, password)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [
                            info.username,
                            hashed_password
                        ]
                    )
                    id = result.fetchone()[0]
                    old_data = info.dict()
                    old_data['hashed_password'] = hashed_password
                    del old_data['password']
                    account = AccountOutWithPassword(id=id, **old_data)
                    return account

        except Exception as e:
            print(e)
            return {'message': 'Could not create user'}
